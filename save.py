import csv
import os
import sqlite3

# Populating and making the database
def make_database(csv_file_name):
    # Making db in current dir
    current_file_path = os.path.dirname(os.path.abspath(__file__))
    db = sqlite3.connect(os.path.join(current_file_path, csv_file_name + '.db'))
    cursor = db.cursor()

    # create the table
    cursor.execute('''
        CREATE TABLE IF NOT EXISTS readings (
            datetime TEXT,
            temperature REAL,
            humidity REAL
        )
    ''')

    # reading the csv file
    with open(os.path.join(current_file_path, csv_file_name + '.csv'), 'r') as f:
        reader = csv.reader(f)
        next(reader) # skip the header row
        to_db = [(row[0], row[1], row[2]) for row in reader]

    # inserting csv data into the table
    cursor.executemany('INSERT INTO readings VALUES (?,?,?)', to_db)

    # saving changes
    db.commit()

    # closeing connection
    db.close()

# A function to show all data in the database in the terminal
def show_data(db_name):
    # Get and connect db in current dir
    current_file_path = os.path.dirname(os.path.abspath(__file__))
    db = sqlite3.connect(os.path.join(current_file_path, '{}.db'.format(db_name)))
    cursor = db.cursor()

    # Show data
    cursor.execute("SELECT * FROM readings")
    rows = cursor.fetchall()
    for row in rows:
        print(row)

    # Close connection
    db.close()



make_database('kolkata_data')
# show_data('kolkata_data')