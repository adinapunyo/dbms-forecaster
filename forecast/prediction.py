import sqlite3
import pandas as pd
import os
import numpy as np
import pickle

from datetime import datetime

from django.conf import settings

humi_sc = pickle.load(open('scalers/humidity_scaler.pkl', 'rb'))
temp_sc = pickle.load(open('scalers/temperature_scaler.pkl', 'rb'))

temp_pred = pickle.load(open('models/temp_linear.pkl', 'rb'))
humi_pred = pickle.load(open('models/humi_linear.pkl', 'rb'))


def read_db(db_name, table_name):
    # Get and connect db in current dir
    current_file_path = os.path.dirname(os.path.abspath('__file__'))
    db = sqlite3.connect(os.path.join(current_file_path, '{}.db'.format(db_name)))
    cursor = db.cursor()

    # Read data from the table
    cursor.execute('SELECT * FROM {}'.format(table_name))
    rows = cursor.fetchall()

    # Create a pandas dataframe
    df = pd.DataFrame(rows, columns=['DATETIME', 'TEMPERATURE', 'HUMIDITY'])

    # Close connection
    db.close()

    return df

# Given a dataframe, write a function to update the database with the data from the dataframe.
def write_db(df, db_name, table_name):
    # Get and connect db in current dir
    current_file_path = os.path.dirname(os.path.abspath('__file__'))
    db = sqlite3.connect(os.path.join(current_file_path, '{}.db'.format(db_name)))
    cursor = db.cursor()

    # Write data to the table
    df.to_sql(table_name, db, if_exists='append', index=False)

    # Close connection
    db.close()

def pred_temp(date_time):
    timestamp = date_time

    pred = [[timestamp]]
    pred = temp_sc.transform(pred)

    temp_pred.predict(pred)
    return temp_sc.inverse_transform(temp_pred.predict(pred))

def pred_humi(date_time):
    timestamp = date_time

    pred = [[timestamp]]
    pred = humi_sc.transform(pred)

    humi_pred.predict(pred)
    return humi_sc.inverse_transform(humi_pred.predict(pred))


def get_timestamp(given_datetime):
    first_datetime = datetime(2015, 1, 1, 0, 0)
    deltatime = given_datetime - first_datetime

    # Get total hours in deltatime
    total_hours = deltatime.total_seconds() // 3600

    return total_hours