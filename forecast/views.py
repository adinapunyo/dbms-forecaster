from django.shortcuts import render
from django.http import HttpResponse

from datetime import datetime

from django.conf import settings
from . import prediction

import pandas as pd

def index(request):
    df = prediction.read_db(settings.DB_NAME, settings.DB_TABLE)

    return render(request, 'index.html')
    
def predict(request):
    if request.method == 'POST':
        given = request.POST.get('datetime')
        given_datetime = datetime.strptime(given, '%Y-%m-%dT%H:%M')
        df = prediction.read_db(settings.DB_NAME, settings.DB_TABLE)

        message = ""

        if given_datetime < datetime(2020, 12, 31, 0, 0):
            # Return temperature from df instead of predicting
            temp = df.loc[df['DATETIME'] == given_datetime.strftime('%#m/%#d/%Y %#H:00'), 'TEMPERATURE'].values[0]
            humi = df.loc[df['DATETIME'] == given_datetime.strftime('%#m/%#d/%Y %#H:00'), 'HUMIDITY'].values[0]

            message = "Entry found in Database"
            print("Found in Database: ", temp, humi)
        else:
            hours = prediction.get_timestamp(given_datetime)
            temp = round(prediction.pred_temp(hours)[0][0], 2)
            humi = round(prediction.pred_humi(hours)[0][0], 2)
            message = "Entry forecasted and saved to Database"

            modified_datetime = given_datetime.strftime('%#m/%#d/%Y %#H:00')

            # Append new data in the old database
            prediction.write_db(pd.DataFrame([[modified_datetime, temp, humi]], columns=['DATETIME', 'TEMPERATURE', 'HUMIDITY']), settings.DB_NAME, settings.DB_TABLE)

            # Append new data in the new database for predicted values only!
            prediction.write_db(pd.DataFrame([[modified_datetime, temp, humi]], columns=['DATETIME', 'TEMPERATURE', 'HUMIDITY']), settings.NEW_DB_NAME, settings.NEW_DB_TABLE)
            print("Predicted and saved to Database: ", temp, humi)


        return render(request, 'predict.html', {'humi':humi, 'temp':temp, 'message':message, 'datetime':given_datetime})
    else:
        return HttpResponse("Invalid request method!")